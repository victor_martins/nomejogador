package br.sc.senai.Auladia22defevereiro;
import javax.inject.Named;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;

@Named(value="testeController")
@SessionScoped
public class PaginainicialController implements Serializable {

private static final long serialVersionUID = 1L;

private String nomeJogador;
private String sobreNome;
private String idade;

public String getSobreNome() {
	return sobreNome;
}

public void setSobreNome(String sobreNome) {
	this.sobreNome = sobreNome;
}

public String getIdade() {
	return idade;
}

public void setIdade(String idade) {
	this.idade = idade;
}

public String getNomeJogador() {
	return nomeJogador;
}

public void setNomeJogador(String nomeJogador) {
	this.nomeJogador = nomeJogador;
}

}